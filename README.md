AA/2018/34789      Cometary topography and phase darkening       (Vincent, 2019)
================================================================================

The associated file "comet_topo.blend" contains the python code and software
environment used to generate virtual comets in Vincent et al, A&A, 2019

The code relies on the internal Python environment provided by Blender
(free and open-source 3D modeling software available at www.blender.org).
To avoid path issues, the code is embedded into the into the .blend file
and must be run from within Blender.

Upon opening, "comet_topo.blend" with Blender 2.8,you will be presented with all
relevant instructions to run, modify, or export the code.

[Example image](https://bitbucket.org/Once/comet_topography/src/master/example_output.png)
